/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Webservices;

import configuracion.DBHelper;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author jorge
 */
@WebService(serviceName = "UsuarioWS")
public class UsuarioWS {
    DBHelper db = new DBHelper();
   
    
    @WebMethod(operationName = "agregarUsuario")
    public boolean agregarUsuario(
            @WebParam(name = "fullname") String fullname,
            @WebParam(name = "email") String email,
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    ) {
        
       try {
            String query =String.format("INSERT INTO user (fullname,email,username,password) "
                           + " VALUES ('%s',"
                           + "'%s',"
                           + "'%s',"
                           + " AES_ENCRYPT('%s','key') )",
                           fullname,email,username,password);
                   
            if(db.connect()){
                return (boolean) db.execute(query, true);
            }
        } catch (Exception e) {
            
        }finally{
            db.disconnect();
        }
        
        return false;
    }
    
    
    @WebMethod(operationName = "editarUsuario")
    public boolean editarUsuario(
            @WebParam(name = "fullname") String fullname,
            @WebParam(name = "email") String email,
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password,
            @WebParam(name = "id") int id
    ) {
        
       try {
            String query =String.format("UPDATE user SET"
                           + "fullname = '%s',"
                           + "email = '%s',"
                           + "username = '%s',"
                           + "password =  AES_ENCRYPT('%s','key') WHERE id = %d",
                           fullname,email,username,password,id);
                   
            if(db.connect()){
                return (boolean) db.execute(query, true);
            }
        } catch (Exception e) {
            
        }finally{
            db.disconnect();
        }
        
        return false;
    }
    
    @WebMethod(operationName = "eliminarUsuario")
    public boolean eliminarUsuario(
            @WebParam(name = "id") int id
    ) {
        
       try {
            String query =String.format("DELETE FROM user WHERE id = %d",id);
                   
            if(db.connect()){
                return (boolean) db.execute(query, true);
            }
        } catch (Exception e) {
            
        }finally{
            db.disconnect();
        }
        
        return false;
    }

}
