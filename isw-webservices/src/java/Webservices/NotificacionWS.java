/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Webservices;

import Entidades.Notificacion;
import configuracion.DBHelper;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author jorge
 */
@WebService(serviceName = "NotificacionWS")
public class NotificacionWS {

    DBHelper db = new DBHelper();
    //CRUD
    
    @WebMethod(operationName = "agregarNotificacion")
    public boolean agregarNotificacion(
            @WebParam(name = "idCurso") int idCurso,
            @WebParam(name = "titulo") String titulo,
            @WebParam(name = "descripcion") String descripcion,
            @WebParam(name = "fecha") String fecha
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("INSERT INTO notificacion (idCurso,titulo,descripcion,fecha)"
                        + " VALUES (%d,'%s','%s','%s');",
                        idCurso,titulo,descripcion,fecha);
                resultado = (boolean) db.execute(query, true);
                
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "editarNotificacion")
    public boolean editarNotificacion(
            @WebParam(name = "idCurso") int idCurso,
            @WebParam(name = "titulo") String titulo,
            @WebParam(name = "descripcion") String descripcion,
            @WebParam(name = "fecha") String fecha,
            @WebParam(name = "id") int id
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("UPDATE notificacion SET "
                        + "idCurso = %d, "
                        + "titulo='%s', descripcion = '%s',"
                        + "fecha = '%s'"
                        + " WHERE id = %d;",
                        idCurso, titulo, descripcion,fecha,
                        id);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "eliminarNotificacion")
    public boolean eliminarNotificacion(
            @WebParam(name = "id") int id
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("DELETE FROM notificacion  WHERE id = %d;",id);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "buscarNotificacions")
    public List<Notificacion> buscarNotificacions(
            @WebParam(name="idUser") int idUser
    ) {
       List<Notificacion> notificaciones = new ArrayList();
       
        try {
            if(db.connect()){
                String query = "SELECT "
                        + " notificacion.id ,"+
                        "  notificacion.titulo, " +
                        "  notificacion.descripcion," +
                        "  notificacion.fecha," +
                        "  notificacion_user.leido "
                        + " FROM notificacion "
                        + " LEFT JOIN notificacion_user ON notificacion.id = notificacion_user.idNotificacion AND notificacion_user.idUser = "+idUser;
                ResultSet rs = (ResultSet) db.execute(query, false);
                while(rs.next()){
                    Notificacion n = new Notificacion();
                    n.setId(rs.getInt("id"));
                    n.setTitulo(rs.getString("titulo"));
                    n.setDescripcion(rs.getString("descripcion"));
                    n.setFecha(rs.getString("fecha"));
                    n.setLeido(rs.getBoolean("leido"));
                    notificaciones.add(n);
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return notificaciones;
       
    }
    
    @WebMethod(operationName = "buscarNotificacionId")
    public Notificacion buscarNotificacionId(
            @WebParam(name = "id") int id,
            @WebParam(name = "idUser") int idUser
    ) {
        
        Notificacion n = new Notificacion();
       
        try {
            if(db.connect()){
                String query = "SELECT "
                        + " notificacion.id ,"+
                        "  notificacion.titulo, " +
                        "  notificacion.descripcion," +
                        "  notificacion.fecha," +
                        "  notificacion_user.leido "
                        + " FROM notificacion "
                        + " LEFT JOIN notificacion_user ON notificacion.id = notificacion_user.idNotificacion AND notificacion_user.idUser = "+idUser+
                        " WHERE id = "+id;
                ResultSet rs = (ResultSet) db.execute(query, false);
                if(rs.next()){
                    n.setId(rs.getInt("id"));
                    n.setTitulo(rs.getString("titulo"));
                    n.setDescripcion(rs.getString("descripcion"));
                    n.setFecha(rs.getString("fecha"));
                    n.setLeido(rs.getBoolean("leido"));
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return n;
    }
    
    
    @WebMethod(operationName = "buscarNotificacionsPor")
    public List<Notificacion> buscarNotificacionsPor(
            @WebParam(name = "tipo") String tipo,
            @WebParam(name = "dato") String dato,
            @WebParam(name = "idUser") int idUser
            
    ) {
         List<Notificacion> notificaciones = new ArrayList();
       
        try {
            if(db.connect()){
                String query = "SELECT "
                        + " notificacion.id ,"+
                        "  notificacion.titulo, " +
                        "  notificacion.descripcion," +
                        "  notificacion.fecha," +
                        "  notificacion_user.leido "
                        + " FROM notificacion "
                        + " LEFT JOIN notificacion_user ON notificacion.id = notificacion_user.idNotificacion AND notificacion_user.idUser = "+idUser+
                        " WHERE "+tipo+" LIKE '%"+dato+"%'";
                ResultSet rs = (ResultSet) db.execute(query, false);
                while(rs.next()){
                    Notificacion n = new Notificacion();
                    n.setId(rs.getInt("id"));
                    n.setTitulo(rs.getString("titulo"));
                    n.setDescripcion(rs.getString("descripcion"));
                    n.setFecha(rs.getString("fecha"));
                    n.setLeido(rs.getBoolean("leido"));
                    notificaciones.add(n);
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return notificaciones;
    }
    
    
}
