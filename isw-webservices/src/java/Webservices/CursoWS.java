
package Webservices;

import Entidades.Curso;
import configuracion.DBHelper;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@WebService(serviceName = "CursoWS")
public class CursoWS {

    DBHelper db = new DBHelper();
    //CRUD
    
    @WebMethod(operationName = "agregarCurso")
    public boolean agregarCurso(
            @WebParam(name = "idUser") int idUser,
            @WebParam(name = "idCategoria") int idCategoria,
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion,
            @WebParam(name = "precio") double precio,
            @WebParam(name = "imagen") String imagen,
            @WebParam(name = "fechaCreacion") String fechaCreacion,
            @WebParam(name = "fechaModificacion") String fechaModificacion,            
            @WebParam(name = "conttype") String conttype,
            @WebParam(name = "favorito") boolean favorito
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("INSERT INTO curso (idUser,idCategoria,nombre,descripcion,"
                        + "precio,imagen,fechaCreacion,fechaModificacion,conttype)"
                        + " VALUES (%d,%d,'%s','%s',%f,'%s','%s','%s','%s');",
                        idUser,idCategoria,nombre,descripcion,
                        precio,imagen,fechaCreacion,fechaModificacion,
                        conttype);
                resultado = (boolean) db.execute(query, true);
                int idCurso = -1;
                
                /* ESTO SOLO ES UN EJEMPLO DE COMO RECUPERAR UN ID RECIEN INSERTADO PARA AGREGARLO
                EN OTRA QUERY   (EN ESTE EJEMPLO NUNCA SUCEDERÁ)*/
                if(favorito && resultado){
                    String queryCurso = String.format("SELECT id FROM cursos "
                            + "WHERE nombre  LIKE '%s' AND fechaCreacion LIKE '%s'",nombre,fechaCreacion);
                    ResultSet rs = (ResultSet) db.execute(queryCurso, false);
                    if(rs.next()){
                        idCurso = rs.getInt("id");
                    }
                    
                    String queryFav = String.format("INSERT INTO favoritos (idUser,idCurso) VALUES (%d,%d);",
                            idUser,idCurso);

                    resultado = (boolean) db.execute(queryFav, true);
                }
                
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "editarCurso")
    public boolean editarCurso(
            @WebParam(name = "idUser") int idUser,
            @WebParam(name = "idCategoria") int idCategoria,
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion,
            @WebParam(name = "precio") double precio,
            @WebParam(name = "imagen") String imagen,
            @WebParam(name = "fechaModificacion") String fechaModificacion,            
            @WebParam(name = "conttype") String conttype,
            @WebParam(name = "id") int id
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("UPDATE curso SET "
                        + "idUser = %d, idCategoria = %d, "
                        + "nombre='%s', descripcion = '%s',"
                        + "precio = %f, imagen =  '%s',"
                        + "fechaModificacion = '%s',conttype = '%s' "
                        + " WHERE id = %d;",
                        idUser, idCategoria,
                        nombre, descripcion,
                        precio, imagen,
                        fechaModificacion, conttype,
                        id);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "eliminarCurso")
    public boolean eliminarCurso(
            @WebParam(name = "id") int id
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("DELETE FROM curso  WHERE id = %d;",id);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "buscarCursos")
    public List<Curso> buscarCursos(
            @WebParam(name="idUser") int idUser
    ) {
       List<Curso> cursos = new ArrayList();
       
        try {
            if(db.connect()){
                String query = "SELECT "
                        + " cursos.id AS 'idCurso' ,"+
                        "  cursos.idUser AS 'idPropietario', " +
                        "  cursos.idCategoria," +
                        "  cursos.nombre," +
                        "  cursos.descripcion," +
                        "  cursos.precio," +
                        "  cursos.imagen," +
                        "  cursos.fechaCreacion," +
                        "  cursos.fechaModificacion," +
                        "  cursos.conttype, "
                        + " favoritos.id AS 'idFavorito' "
                        + " FROM cursos "
                        + " LEFT JOIN favoritos ON cursos.id = favoritos.idCurso AND favoritos.idUser = "+idUser;
                ResultSet rs = (ResultSet) db.execute(query, false);
                while(rs.next()){
                    Curso c = new Curso();
                    c.setId(rs.getInt("idCurso"));
                    c.setIdUser(rs.getInt("idPropietario"));
                    c.setIdCategoria(rs.getInt("idCategoria"));
                    c.setNombre(rs.getString("nombre"));
                    c.setDescripcion(rs.getString("descripcion"));
                    c.setPrecio(rs.getDouble("precio"));
                    c.setImagen(rs.getString("imagen"));
                    c.setFechaCreacion(rs.getString("fechaCreacion"));
                    c.setFechaModificacion(rs.getString("fechaModificacion"));
                    c.setConttype(rs.getString("conttype"));
                    c.setFavorito( rs.getInt("idFavorito") > 0 ?true:false  );
                    cursos.add(c);
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return cursos;
       
    }
    
    @WebMethod(operationName = "buscarCursoId")
    public Curso buscarCursoId(
            @WebParam(name = "id") int id,
            @WebParam(name = "idUser") int idUser
    ) {
        
        Curso c = new Curso();
       
        try {
            if(db.connect()){
                String query = "SELECT "
                        + " cursos.id AS 'idCurso' ,"+
                        "  cursos.idUser AS 'idPropietario', " +
                        "  cursos.idCategoria," +
                        "  cursos.nombre," +
                        "  cursos.descripcion," +
                        "  cursos.precio," +
                        "  cursos.imagen," +
                        "  cursos.fechaCreacion," +
                        "  cursos.fechaModificacion," +
                        "  cursos.conttype, "
                        + " favoritos.id AS 'idFavorito' "
                        + " FROM cursos "
                        + " LEFT JOIN favoritos ON cursos.id = favoritos.idCurso AND favoritos.idUser = "+idUser+
                        " WHERE id = "+id;
                ResultSet rs = (ResultSet) db.execute(query, false);
                if(rs.next()){
                    c.setId(rs.getInt("idCurso"));
                    c.setIdUser(rs.getInt("idPropietario"));
                    c.setIdCategoria(rs.getInt("idCategoria"));
                    c.setNombre(rs.getString("nombre"));
                    c.setDescripcion(rs.getString("descripcion"));
                    c.setPrecio(rs.getDouble("precio"));
                    c.setImagen(rs.getString("imagen"));
                    c.setFechaCreacion(rs.getString("fechaCreacion"));
                    c.setFechaModificacion(rs.getString("fechaModificacion"));
                    c.setConttype(rs.getString("conttype"));
                    c.setFavorito( rs.getInt("idFavorito") > 0 ?true:false  );
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return c;
    }
    
    
    @WebMethod(operationName = "buscarCursosPor")
    public List<Curso> buscarCursosPor(
            @WebParam(name = "tipo") String tipo,
            @WebParam(name = "dato") String dato,
            @WebParam(name = "idUser") int idUser
            
    ) {
         List<Curso> cursos = new ArrayList();
       
        try {
            if(db.connect()){
                String query = "SELECT "
                        + " cursos.id AS 'idCurso' ,"+
                        "  cursos.idUser AS 'idPropietario', " +
                        "  cursos.idCategoria," +
                        "  cursos.nombre," +
                        "  cursos.descripcion," +
                        "  cursos.precio," +
                        "  cursos.imagen," +
                        "  cursos.fechaCreacion," +
                        "  cursos.fechaModificacion," +
                        "  cursos.conttype, "
                        + " favoritos.id AS 'idFavorito' "
                        + " FROM cursos "
                        + " LEFT JOIN favoritos ON cursos.id = favoritos.idCurso AND favoritos.idUser = "+idUser+
                        " WHERE "+tipo+" LIKE '%"+dato+"%'";
                ResultSet rs = (ResultSet) db.execute(query, false);
                while(rs.next()){
                    Curso c = new Curso();
                    c.setId(rs.getInt("idCurso"));
                    c.setIdUser(rs.getInt("idPropietario"));
                    c.setIdCategoria(rs.getInt("idCategoria"));
                    c.setNombre(rs.getString("nombre"));
                    c.setDescripcion(rs.getString("descripcion"));
                    c.setPrecio(rs.getDouble("precio"));
                    c.setImagen(rs.getString("imagen"));
                    c.setFechaCreacion(rs.getString("fechaCreacion"));
                    c.setFechaModificacion(rs.getString("fechaModificacion"));
                    c.setConttype(rs.getString("conttype"));
                    c.setFavorito( rs.getInt("idFavorito") > 0 ?true:false  );
                    cursos.add(c);
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return cursos;
    }
    
    
    
}
