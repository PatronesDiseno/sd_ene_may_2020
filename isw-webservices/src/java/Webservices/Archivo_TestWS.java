/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Webservices;

import Entidades.Archivo_Test;
import configuracion.DBHelper;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author jorge
 */
@WebService(serviceName = "Archivo_TestWS")
public class Archivo_TestWS {

    DBHelper db = new DBHelper();
    //CRUD
   
    @WebMethod(operationName = "agregarArchivo_Test")
    public boolean agregarArchivo_Test(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "contenttype") String contenttype,
            @WebParam(name = "archivo") String archivo
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("INSERT INTO archivo_test (nombre,contenttype,archivo) VALUES ('%s','%s','%s');", nombre,contenttype,archivo);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "editarArchivo_Test")
    public boolean editarArchivo_Test(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "contenttype") String contenttype,
            @WebParam(name = "archivo") String archivo,
            @WebParam(name = "id") int id
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("UPDATE archivo_test SET nombre='%s',contenttype = '%s', archivo = '%s' WHERE id = %d;", nombre,contenttype,archivo,id);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "eliminarArchivo_Test")
    public boolean eliminarArchivo_Test(
            @WebParam(name = "id") int id
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("DELETE FROM archivo_test  WHERE id = %d;",id);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "buscarArchivo_Tests")
    public List<Archivo_Test> buscarArchivo_Tests(
    ) {
       List<Archivo_Test> archivo_tests = new ArrayList();
       
        try {
            if(db.connect()){
                String query = "SELECT * FROM archivo_test";
                ResultSet rs = (ResultSet) db.execute(query, false);
                while(rs.next()){
                    Archivo_Test c = new Archivo_Test();
                    c.setId(rs.getInt("id"));
                    c.setNombre(rs.getString("nombre"));
                    c.setContenttype(rs.getString("contenttype"));
                    c.setArchivo(rs.getString("archivo"));
                    archivo_tests.add(c);
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return archivo_tests;
       
    }
    
    @WebMethod(operationName = "buscarArchivo_TestId")
    public Archivo_Test buscarArchivo_TestId(
            @WebParam(name = "id") int id
    ) {
        Archivo_Test c = new Archivo_Test();
       
        try {
            if(db.connect()){
                String query = String.format("SELECT * FROM archivo_test WHERE id=%d",id);
                ResultSet rs = (ResultSet) db.execute(query, false);
                if(rs.next()){
                    c.setId(rs.getInt("id"));
                    c.setNombre(rs.getString("nombre"));
                    c.setContenttype(rs.getString("contenttype"));
                    c.setArchivo(rs.getString("archivo"));
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return c;
    }
    
    
    @WebMethod(operationName = "buscarArchivo_TestsPor")
    public List<Archivo_Test> buscarArchivo_TestsPor(
            @WebParam(name = "tipo") String tipo,
            @WebParam(name = "dato") String dato
    ) {
         List<Archivo_Test> archivo_tests = new ArrayList();
       
        try {
            if(db.connect()){
                String query = String.format("SELECT * FROM archivo_test WHERE %s LIKE '%s' ",tipo,"%"+dato+"%");
                System.out.println(query);
                ResultSet rs = (ResultSet) db.execute(query, false);
                while(rs.next()){
                    Archivo_Test c = new Archivo_Test();
                    c.setId(rs.getInt("id"));
                    c.setNombre(rs.getString("nombre"));
                    c.setContenttype(rs.getString("contenttype"));
                    c.setArchivo(rs.getString("archivo"));
                    archivo_tests.add(c);
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return archivo_tests;
    }
}
