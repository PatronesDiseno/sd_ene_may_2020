package Webservices;

import Entidades.Categoria;
import configuracion.DBHelper;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@WebService(serviceName = "CategoriaWS")
public class CategoriaWS {
    DBHelper db = new DBHelper();
    //CRUD
   
    @WebMethod(operationName = "agregarCategoria")
    public boolean agregarCategoria(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("INSERT INTO categoria (nombre,descripcion) VALUES ('%s','%s');", nombre,descripcion);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "editarCategoria")
    public boolean editarCategoria(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion,
            @WebParam(name = "id") int id
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("UPDATE categoria SET nombre='%s',descripcion = '%s' WHERE id = %d;", nombre,descripcion,id);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "eliminarCategoria")
    public boolean eliminarCategoria(
            @WebParam(name = "id") int id
    ) {
        boolean resultado = false;
        try {
            if(db.connect()){                
                String query = String.format("DELETE FROM categoria  WHERE id = %d;",id);
                resultado = (boolean) db.execute(query, true);
            }            
            
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
        return resultado;
    }
    
    @WebMethod(operationName = "buscarCategorias")
    public List<Categoria> buscarCategorias(
    ) {
       List<Categoria> categorias = new ArrayList();
       
        try {
            if(db.connect()){
                String query = "SELECT * FROM categoria";
                ResultSet rs = (ResultSet) db.execute(query, false);
                while(rs.next()){
                    Categoria c = new Categoria();
                    c.setId(rs.getInt("id"));
                    c.setNombre(rs.getString("nombre"));
                    c.setDescripcion(rs.getString("descripcion"));
                    categorias.add(c);
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return categorias;
       
    }
    
    @WebMethod(operationName = "buscarCategoriaId")
    public Categoria buscarCategoriaId(
            @WebParam(name = "id") int id
    ) {
        Categoria c = new Categoria();
       
        try {
            if(db.connect()){
                String query = String.format("SELECT * FROM categoria WHERE id=%d",id);
                ResultSet rs = (ResultSet) db.execute(query, false);
                if(rs.next()){
                    c.setId(rs.getInt("id"));
                    c.setNombre(rs.getString("nombre"));
                    c.setDescripcion(rs.getString("descripcion"));
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return c;
    }
    
    
    @WebMethod(operationName = "buscarCategoriasPor")
    public List<Categoria> buscarCategoriasPor(
            @WebParam(name = "tipo") String tipo,
            @WebParam(name = "dato") String dato
    ) {
         List<Categoria> categorias = new ArrayList();
       
        try {
            if(db.connect()){
                String query = String.format("SELECT * FROM categoria WHERE %s LIKE '%s' ",tipo,"%"+dato+"%");
                System.out.println(query);
                ResultSet rs = (ResultSet) db.execute(query, false);
                while(rs.next()){
                    Categoria c = new Categoria();
                    c.setId(rs.getInt("id"));
                    c.setNombre(rs.getString("nombre"));
                    c.setDescripcion(rs.getString("descripcion"));
                    categorias.add(c);
                }
            }
        } catch (Exception e) {
        }finally{
            db.disconnect();
        }
       
       return categorias;
    }
    
}
