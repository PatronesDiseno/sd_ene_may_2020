package Webservices;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import Entidades.Usuario;
import configuracion.DBHelper;
import java.sql.ResultSet;

@WebService(serviceName = "login")
public class login {
    
    DBHelper db = new DBHelper();
    
    @WebMethod(operationName = "login")
    public Usuario login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    
    ) {
        
        Usuario user = new Usuario();
        
        try {
            if(db.connect()){
                String query = "SELECT * FROM user WHERE "
                        + "username LIKE '"+username+"' AND "
                        + "password LIKE AES_ENCRYPT('"+password+"','key') ";
                
                ResultSet rs = (ResultSet) db.execute(query, false);
                if(rs.next()){ //Multiples datos o fila uso while
                    user.setFullname(rs.getString("fullname"));
                    user.setEmail(rs.getString("email"));
                    user.setUsername(rs.getString("username"));
//                    user.setPassword(rs.getString("password"));
                    user.setId(rs.getInt("id"));
                }
            }
        } catch (Exception e) {
            //System.out.println("Exception "+e.getMessage());
        }finally{//Siempre cerramos la conexion
            db.disconnect();
        }
        
        return user;
        
        
    }
    
    
       
}
