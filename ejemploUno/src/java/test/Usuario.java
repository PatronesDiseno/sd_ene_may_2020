
package test;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
    private int edad;
    private String nombre;
    private String apellido;
    
    public List<Usuario> usuarios = new ArrayList();

    public Usuario() {
        
        usuarios.add(new Usuario(18,"Andres","Lopez"));
        usuarios.add(new Usuario(25,"Humberto","Lopez"));
        usuarios.add(new Usuario(22,"Fernando","Lopez"));
        usuarios.add(new Usuario(15,"Claudia","Lopez"));
        usuarios.add(new Usuario(13,"Rosela","Lopez"));
        usuarios.add(new Usuario(30,"Guadalupe","Lopez"));
        usuarios.add(new Usuario(58,"Maria","Lopez"));
        usuarios.add(new Usuario(85,"Gustavo","Lopez"));
        usuarios.add(new Usuario(33,"Tifanny","Lopez"));
        usuarios.add(new Usuario(5,"Pedro","Lopez"));
                 
    }

    public Usuario(int edad, String nombre, String apellido) {
        this.edad = edad;
        this.nombre = nombre;
        this.apellido = apellido;
    }
    
    

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    
    
}
