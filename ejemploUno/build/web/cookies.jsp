<%-- 
    Document   : cookies
    Created on : 7/02/2020, 02:19:13 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            //Se define la cookie pero aun no se asigna
            Cookie c1 = new Cookie("usuario", "Jorge");
            //Seteamos una duracion en segundos
            c1.setMaxAge(60*5);
            
            //Asignar la cookie
            response.addCookie(c1); 
        
        
        %>
    </body>
</html>
