
package loginWS;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.11-b150120.1832
 * Generated source version: 2.2
 * 
 */
@WebService(name = "login", targetNamespace = "http://Webservices/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface Login {


    /**
     * 
     * @param password
     * @param username
     * @return
     *     returns loginWS.Usuario
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "login", targetNamespace = "http://Webservices/", className = "loginWS.Login_Type")
    @ResponseWrapper(localName = "loginResponse", targetNamespace = "http://Webservices/", className = "loginWS.LoginResponse")
    @Action(input = "http://Webservices/login/loginRequest", output = "http://Webservices/login/loginResponse")
    public Usuario login(
        @WebParam(name = "username", targetNamespace = "")
        String username,
        @WebParam(name = "password", targetNamespace = "")
        String password);

}
