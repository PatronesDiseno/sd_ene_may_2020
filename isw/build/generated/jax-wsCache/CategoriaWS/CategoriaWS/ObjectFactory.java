
package CategoriaWS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the CategoriaWS package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AgregarCategoria_QNAME = new QName("http://Webservices/", "agregarCategoria");
    private final static QName _AgregarCategoriaResponse_QNAME = new QName("http://Webservices/", "agregarCategoriaResponse");
    private final static QName _BuscarCategoriaId_QNAME = new QName("http://Webservices/", "buscarCategoriaId");
    private final static QName _BuscarCategoriaIdResponse_QNAME = new QName("http://Webservices/", "buscarCategoriaIdResponse");
    private final static QName _BuscarCategorias_QNAME = new QName("http://Webservices/", "buscarCategorias");
    private final static QName _BuscarCategoriasPor_QNAME = new QName("http://Webservices/", "buscarCategoriasPor");
    private final static QName _BuscarCategoriasPorResponse_QNAME = new QName("http://Webservices/", "buscarCategoriasPorResponse");
    private final static QName _BuscarCategoriasResponse_QNAME = new QName("http://Webservices/", "buscarCategoriasResponse");
    private final static QName _EditarCategoria_QNAME = new QName("http://Webservices/", "editarCategoria");
    private final static QName _EditarCategoriaResponse_QNAME = new QName("http://Webservices/", "editarCategoriaResponse");
    private final static QName _EliminarCategoria_QNAME = new QName("http://Webservices/", "eliminarCategoria");
    private final static QName _EliminarCategoriaResponse_QNAME = new QName("http://Webservices/", "eliminarCategoriaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: CategoriaWS
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AgregarCategoria }
     * 
     */
    public AgregarCategoria createAgregarCategoria() {
        return new AgregarCategoria();
    }

    /**
     * Create an instance of {@link AgregarCategoriaResponse }
     * 
     */
    public AgregarCategoriaResponse createAgregarCategoriaResponse() {
        return new AgregarCategoriaResponse();
    }

    /**
     * Create an instance of {@link BuscarCategoriaId }
     * 
     */
    public BuscarCategoriaId createBuscarCategoriaId() {
        return new BuscarCategoriaId();
    }

    /**
     * Create an instance of {@link BuscarCategoriaIdResponse }
     * 
     */
    public BuscarCategoriaIdResponse createBuscarCategoriaIdResponse() {
        return new BuscarCategoriaIdResponse();
    }

    /**
     * Create an instance of {@link BuscarCategorias }
     * 
     */
    public BuscarCategorias createBuscarCategorias() {
        return new BuscarCategorias();
    }

    /**
     * Create an instance of {@link BuscarCategoriasPor }
     * 
     */
    public BuscarCategoriasPor createBuscarCategoriasPor() {
        return new BuscarCategoriasPor();
    }

    /**
     * Create an instance of {@link BuscarCategoriasPorResponse }
     * 
     */
    public BuscarCategoriasPorResponse createBuscarCategoriasPorResponse() {
        return new BuscarCategoriasPorResponse();
    }

    /**
     * Create an instance of {@link BuscarCategoriasResponse }
     * 
     */
    public BuscarCategoriasResponse createBuscarCategoriasResponse() {
        return new BuscarCategoriasResponse();
    }

    /**
     * Create an instance of {@link EditarCategoria }
     * 
     */
    public EditarCategoria createEditarCategoria() {
        return new EditarCategoria();
    }

    /**
     * Create an instance of {@link EditarCategoriaResponse }
     * 
     */
    public EditarCategoriaResponse createEditarCategoriaResponse() {
        return new EditarCategoriaResponse();
    }

    /**
     * Create an instance of {@link EliminarCategoria }
     * 
     */
    public EliminarCategoria createEliminarCategoria() {
        return new EliminarCategoria();
    }

    /**
     * Create an instance of {@link EliminarCategoriaResponse }
     * 
     */
    public EliminarCategoriaResponse createEliminarCategoriaResponse() {
        return new EliminarCategoriaResponse();
    }

    /**
     * Create an instance of {@link Categoria }
     * 
     */
    public Categoria createCategoria() {
        return new Categoria();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarCategoria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "agregarCategoria")
    public JAXBElement<AgregarCategoria> createAgregarCategoria(AgregarCategoria value) {
        return new JAXBElement<AgregarCategoria>(_AgregarCategoria_QNAME, AgregarCategoria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarCategoriaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "agregarCategoriaResponse")
    public JAXBElement<AgregarCategoriaResponse> createAgregarCategoriaResponse(AgregarCategoriaResponse value) {
        return new JAXBElement<AgregarCategoriaResponse>(_AgregarCategoriaResponse_QNAME, AgregarCategoriaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarCategoriaId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarCategoriaId")
    public JAXBElement<BuscarCategoriaId> createBuscarCategoriaId(BuscarCategoriaId value) {
        return new JAXBElement<BuscarCategoriaId>(_BuscarCategoriaId_QNAME, BuscarCategoriaId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarCategoriaIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarCategoriaIdResponse")
    public JAXBElement<BuscarCategoriaIdResponse> createBuscarCategoriaIdResponse(BuscarCategoriaIdResponse value) {
        return new JAXBElement<BuscarCategoriaIdResponse>(_BuscarCategoriaIdResponse_QNAME, BuscarCategoriaIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarCategorias }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarCategorias")
    public JAXBElement<BuscarCategorias> createBuscarCategorias(BuscarCategorias value) {
        return new JAXBElement<BuscarCategorias>(_BuscarCategorias_QNAME, BuscarCategorias.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarCategoriasPor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarCategoriasPor")
    public JAXBElement<BuscarCategoriasPor> createBuscarCategoriasPor(BuscarCategoriasPor value) {
        return new JAXBElement<BuscarCategoriasPor>(_BuscarCategoriasPor_QNAME, BuscarCategoriasPor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarCategoriasPorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarCategoriasPorResponse")
    public JAXBElement<BuscarCategoriasPorResponse> createBuscarCategoriasPorResponse(BuscarCategoriasPorResponse value) {
        return new JAXBElement<BuscarCategoriasPorResponse>(_BuscarCategoriasPorResponse_QNAME, BuscarCategoriasPorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarCategoriasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarCategoriasResponse")
    public JAXBElement<BuscarCategoriasResponse> createBuscarCategoriasResponse(BuscarCategoriasResponse value) {
        return new JAXBElement<BuscarCategoriasResponse>(_BuscarCategoriasResponse_QNAME, BuscarCategoriasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditarCategoria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "editarCategoria")
    public JAXBElement<EditarCategoria> createEditarCategoria(EditarCategoria value) {
        return new JAXBElement<EditarCategoria>(_EditarCategoria_QNAME, EditarCategoria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditarCategoriaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "editarCategoriaResponse")
    public JAXBElement<EditarCategoriaResponse> createEditarCategoriaResponse(EditarCategoriaResponse value) {
        return new JAXBElement<EditarCategoriaResponse>(_EditarCategoriaResponse_QNAME, EditarCategoriaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarCategoria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "eliminarCategoria")
    public JAXBElement<EliminarCategoria> createEliminarCategoria(EliminarCategoria value) {
        return new JAXBElement<EliminarCategoria>(_EliminarCategoria_QNAME, EliminarCategoria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarCategoriaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "eliminarCategoriaResponse")
    public JAXBElement<EliminarCategoriaResponse> createEliminarCategoriaResponse(EliminarCategoriaResponse value) {
        return new JAXBElement<EliminarCategoriaResponse>(_EliminarCategoriaResponse_QNAME, EliminarCategoriaResponse.class, null, value);
    }

}
