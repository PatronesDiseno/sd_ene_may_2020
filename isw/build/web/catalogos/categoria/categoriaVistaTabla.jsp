<%-- 
    Document   : categoriaVistaTabla
    Created on : 27/03/2020, 06:13:43 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="../../js/categoria.js" type="text/javascript"></script>
        <link href="../../css/catalogos/categoria/categoria.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor-menu">
            <table>
                <tr>
                    <td colspan="2" class="direccion">
                        / isw / catalogos / categoria / Vista Tabla 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a onclick="agregar()" class="boton">Agregar</a>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
       
        <table id="vistaTabla">
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DESCRIPCION</th>
            </tr>
        <%
        try {
            CategoriaWS.CategoriaWS_Service service = new CategoriaWS.CategoriaWS_Service();
            CategoriaWS.CategoriaWS port = service.getCategoriaWSPort();
            // TODO process result here
            java.util.List<CategoriaWS.Categoria> categorias = port.buscarCategorias();

            if(categorias.size()> 0){
                for(CategoriaWS.Categoria categoria : categorias){
                    %>
                    <tr onclick="vistaIndividual('<%= categoria.getId() %>')" >
                        <td><%= categoria.getId() %></td>
                        <td><%= categoria.getNombre() %> </td>
                        <td><%= categoria.getDescripcion() %></td>
                    </tr>

                    <%
                }
            }else{
//                out.print("");
            %>
            <tr>
                <td colspan="3">No se encontraron Registros...</td>
            </tr>
            
            <%
            }

        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }
        %>


        
       
        </table>  
    </body>
</html>
