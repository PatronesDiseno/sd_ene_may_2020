<%-- 
    Document   : categoriaVistaIndividual
    Created on : 27/03/2020, 06:14:02 PM
    Author     : jorge
--%>


<%
    
    int id = 0;
    CategoriaWS.Categoria categoria = null;
    
    try {
        id = Integer.valueOf( request.getParameter("id"));
       
	CategoriaWS.CategoriaWS_Service service = new CategoriaWS.CategoriaWS_Service();
	CategoriaWS.CategoriaWS port = service.getCategoriaWSPort();
        
	categoria = port.buscarCategoriaId(id);
	
    } catch (Exception e) {
    }


    
%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="../../css/catalogos/categoria/categoria.css" rel="stylesheet" type="text/css"/>
        <script src="../../js/categoria.js" type="text/javascript"></script>
    </head>
    <body>
        
        <div class="contenedor-menu">
            <table>
                <tr>
                    <td colspan="2" class="direccion">
                        / isw / catalogos / categoria / Vista Formulario 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a onclick="guardar()" class="boton">Guardar</a>
                    </td>
                    <td>
                        <a onclick="cancelar()" class="boton">Cancelar</a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="contenedor-principal">
            <form id="formulario" action="/isw/CategoriaServlet" method="GET">
                <input type="hidden" name="id" value="<%= id %>"/>
                <input type="hidden" name="opcion" value="<%= id >-1 ? "editar":"agregar"  %>"/>
                <div class="contenedor-targeta">                    
                    <div class="header">
                        <%= categoria != null ? categoria.getNombre(): "N/A"  %>
                    </div>
                    <div class="contenido">
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Nombre:</td>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="text" name="nombre" value="<%= categoria != null ? categoria.getNombre(): "N/A"  %>" />
                                      </td>
                            </tr>

                            <tr>
                                <td>Descripción: </td>
                                <td>&nbsp;</td>
                                <td>
                                    <textarea name="descripcion"><%= categoria != null ? categoria.getDescripcion(): "N/A"  %></textarea>
                                </td>
                            </tr>
                        </table>

                    </div>
                    <div class="footer">
                        &nbsp;
                    </div>

                </div>
            </form>
            
        </div>
        
        
    </body>
</html>
