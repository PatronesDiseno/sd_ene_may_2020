<%@page import="java.io.*" %>
<%
    

    int id = -1;

    try {
       id  = Integer.valueOf(request.getParameter("id")); //id archivo
       
    } catch (Exception e) {
    }

    if(id >-1){
       ServletOutputStream outputStream = null;
       
       try{
        //Recuperar datos del archivo
        
	Archivo_TestWS.ArchivoTestWS_Service service = new Archivo_TestWS.ArchivoTestWS_Service();
	Archivo_TestWS.ArchivoTestWS port = service.getArchivoTestWSPort();
	
	Archivo_TestWS.ArchivoTest archivo = port.buscarArchivoTestId(id);
	
        byte[] _archivo = javax.xml.bind.DatatypeConverter.parseBase64Binary(archivo.getArchivo());
        
        
        response.setContentType(archivo.getContenttype());
        response.setHeader("cache-control", "no-cache");
        response.setHeader("Content-Disposition", "inline; filename= "+archivo.getNombre());
        response.setContentLength(_archivo.length);
        
        out.clear(); // Se borra la configuracion por defecto
        
        outputStream = response.getOutputStream(); //Toma la nueva configuracion 
                                                   //Asignada por nosotros.
                                                   
        outputStream.write(_archivo);
       }catch(Exception ex){
                
       }finally{
           outputStream.flush();
           outputStream.close();
       }    
        
    }



%>