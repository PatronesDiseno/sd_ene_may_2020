<%-- 
    Document   : bienvenido
    Created on : 17/02/2020, 02:31:34 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/nabvar.css" rel="stylesheet" type="text/css"/>
        <link href="css/bienvenido.css" rel="stylesheet" type="text/css"/>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/ad9b69165e.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light" >
            <a class="navbar-brand" href="#">
                <img src="/isw-academy/img/logo-linea.png" alt="" height="35" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link categories" href="#">
                            <i class="fas fa-th"></i>
                            Categories
                        </a>
                    </li>
                    <li class="nav-item ">
                        <form class="form-inline  my-2 my-lg-0">
                            <div class="search">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search your course" aria-label="Search" />
                                <i class="fas fa-search" ></i>
                            </div>
                        </form>
                    </li>

                    <li class="nav-item ">
                        <h:form>
                            <a href="#" class="nav-link my-courses" >My Courses</a>
                        </h:form>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="#"  >
                            <i class="far fa-heart favoritos"></i>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link notifications" href="#"  >
                            <i class="far fa-bell"></i>
                            <span>
                                2
                            </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link profile" href="#"  >
                            <i class="far fa-user"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        
        
         <div class="container">
            <div class="row">
                <div class="col-md-12 offer">
                    <img src="img/especial-offer.jpg" alt="" width="100%"/>
                    <div class="offer-text">
                        <h3>Especial Offer</h3>
                        <p>A new Course of JSF 2.0 is Available only for</p>
                        <span class="price">$1</span>
                    </div>
                </div>
            </div>
             
             
         </div>
        
        
    </body>
</html>
