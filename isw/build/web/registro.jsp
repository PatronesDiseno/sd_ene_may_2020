<%-- 
    Document   : registro
    Created on : 26/02/2020, 01:43:58 PM
    Author     : jorge
--%>



<%@page import="UsuarioWS.UsuarioWS"%>
<%@page import="UsuarioWS.UsuarioWS_Service"%>
<%
    String error = "";
    if(request.getParameter("username")!=null){
    
        try {
           UsuarioWS_Service service = new UsuarioWS_Service();
           UsuarioWS port = service.getUsuarioWSPort();
             // TODO initialize WS operation arguments here
            String fullname = request.getParameter("fullname");
            String email = request.getParameter("email");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            // TODO process result here
            boolean result = port.agregarUsuario(fullname, email, username, password);
            
            if(result){
                response.sendRedirect("index.jsp");
            }else{
                error = "Algo salio mal, contacte a Administrador";
            }
            
            
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }

    }
    
    
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/registrar.css" rel="stylesheet" type="text/css"/>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
    </head>
    <body>
        <form action="registro.jsp" method="POST">
            <div class="container">
                <div class="row mt-40">
                    <div class="col-md-4">&nbsp;</div>
                    <div class="col-md-4">
                        <div class="logo-container">
                            <img src="./img/Logo.png" alt=""/>
                        </div>
                        <div class="form-group">
                            <label for="fullname">Fullname <span>*</span></label>
                            <input type="text" class="form-control" name="fullname" id="fullname" value="" required="true" requiredMessage="You need digit the full name of the user"/>
                        </div>
                        <div class="form-group">
                            <label for="email">Email <span>*</span></label>
                            <input class="form-control" name="email" id="email" value="" required="true" requiredMessage="You need digit the email of the user"/>
                        </div>
                        <div class="form-group">
                            <label for="username">Username <span>*</span></label>
                            <input type="text" name="username"  class="form-control" id="username" value="" required="true" requiredMessage="You need digit the username of the user"/>
                        </div>
                        <div class="form-group">
                            <label for="password">Password <span>*</span></label>
                            <input type="password" name="password" class="form-control" id="password" value="" required="true" requiredMessage="You need digit the password of the user"/>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-registrar" value="Register" />
                        </div>
                         <span class="badge badge-danger"><%= error %></span>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
            </div>
        </form>
    </body>
</html>
