
package Archivo_TestWS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the Archivo_TestWS package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AgregarArchivoTest_QNAME = new QName("http://Webservices/", "agregarArchivo_Test");
    private final static QName _AgregarArchivoTestResponse_QNAME = new QName("http://Webservices/", "agregarArchivo_TestResponse");
    private final static QName _BuscarArchivoTestId_QNAME = new QName("http://Webservices/", "buscarArchivo_TestId");
    private final static QName _BuscarArchivoTestIdResponse_QNAME = new QName("http://Webservices/", "buscarArchivo_TestIdResponse");
    private final static QName _BuscarArchivoTests_QNAME = new QName("http://Webservices/", "buscarArchivo_Tests");
    private final static QName _BuscarArchivoTestsPor_QNAME = new QName("http://Webservices/", "buscarArchivo_TestsPor");
    private final static QName _BuscarArchivoTestsPorResponse_QNAME = new QName("http://Webservices/", "buscarArchivo_TestsPorResponse");
    private final static QName _BuscarArchivoTestsResponse_QNAME = new QName("http://Webservices/", "buscarArchivo_TestsResponse");
    private final static QName _EditarArchivoTest_QNAME = new QName("http://Webservices/", "editarArchivo_Test");
    private final static QName _EditarArchivoTestResponse_QNAME = new QName("http://Webservices/", "editarArchivo_TestResponse");
    private final static QName _EliminarArchivoTest_QNAME = new QName("http://Webservices/", "eliminarArchivo_Test");
    private final static QName _EliminarArchivoTestResponse_QNAME = new QName("http://Webservices/", "eliminarArchivo_TestResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: Archivo_TestWS
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AgregarArchivoTest }
     * 
     */
    public AgregarArchivoTest createAgregarArchivoTest() {
        return new AgregarArchivoTest();
    }

    /**
     * Create an instance of {@link AgregarArchivoTestResponse }
     * 
     */
    public AgregarArchivoTestResponse createAgregarArchivoTestResponse() {
        return new AgregarArchivoTestResponse();
    }

    /**
     * Create an instance of {@link BuscarArchivoTestId }
     * 
     */
    public BuscarArchivoTestId createBuscarArchivoTestId() {
        return new BuscarArchivoTestId();
    }

    /**
     * Create an instance of {@link BuscarArchivoTestIdResponse }
     * 
     */
    public BuscarArchivoTestIdResponse createBuscarArchivoTestIdResponse() {
        return new BuscarArchivoTestIdResponse();
    }

    /**
     * Create an instance of {@link BuscarArchivoTests }
     * 
     */
    public BuscarArchivoTests createBuscarArchivoTests() {
        return new BuscarArchivoTests();
    }

    /**
     * Create an instance of {@link BuscarArchivoTestsPor }
     * 
     */
    public BuscarArchivoTestsPor createBuscarArchivoTestsPor() {
        return new BuscarArchivoTestsPor();
    }

    /**
     * Create an instance of {@link BuscarArchivoTestsPorResponse }
     * 
     */
    public BuscarArchivoTestsPorResponse createBuscarArchivoTestsPorResponse() {
        return new BuscarArchivoTestsPorResponse();
    }

    /**
     * Create an instance of {@link BuscarArchivoTestsResponse }
     * 
     */
    public BuscarArchivoTestsResponse createBuscarArchivoTestsResponse() {
        return new BuscarArchivoTestsResponse();
    }

    /**
     * Create an instance of {@link EditarArchivoTest }
     * 
     */
    public EditarArchivoTest createEditarArchivoTest() {
        return new EditarArchivoTest();
    }

    /**
     * Create an instance of {@link EditarArchivoTestResponse }
     * 
     */
    public EditarArchivoTestResponse createEditarArchivoTestResponse() {
        return new EditarArchivoTestResponse();
    }

    /**
     * Create an instance of {@link EliminarArchivoTest }
     * 
     */
    public EliminarArchivoTest createEliminarArchivoTest() {
        return new EliminarArchivoTest();
    }

    /**
     * Create an instance of {@link EliminarArchivoTestResponse }
     * 
     */
    public EliminarArchivoTestResponse createEliminarArchivoTestResponse() {
        return new EliminarArchivoTestResponse();
    }

    /**
     * Create an instance of {@link ArchivoTest }
     * 
     */
    public ArchivoTest createArchivoTest() {
        return new ArchivoTest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarArchivoTest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "agregarArchivo_Test")
    public JAXBElement<AgregarArchivoTest> createAgregarArchivoTest(AgregarArchivoTest value) {
        return new JAXBElement<AgregarArchivoTest>(_AgregarArchivoTest_QNAME, AgregarArchivoTest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarArchivoTestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "agregarArchivo_TestResponse")
    public JAXBElement<AgregarArchivoTestResponse> createAgregarArchivoTestResponse(AgregarArchivoTestResponse value) {
        return new JAXBElement<AgregarArchivoTestResponse>(_AgregarArchivoTestResponse_QNAME, AgregarArchivoTestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarArchivoTestId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarArchivo_TestId")
    public JAXBElement<BuscarArchivoTestId> createBuscarArchivoTestId(BuscarArchivoTestId value) {
        return new JAXBElement<BuscarArchivoTestId>(_BuscarArchivoTestId_QNAME, BuscarArchivoTestId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarArchivoTestIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarArchivo_TestIdResponse")
    public JAXBElement<BuscarArchivoTestIdResponse> createBuscarArchivoTestIdResponse(BuscarArchivoTestIdResponse value) {
        return new JAXBElement<BuscarArchivoTestIdResponse>(_BuscarArchivoTestIdResponse_QNAME, BuscarArchivoTestIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarArchivoTests }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarArchivo_Tests")
    public JAXBElement<BuscarArchivoTests> createBuscarArchivoTests(BuscarArchivoTests value) {
        return new JAXBElement<BuscarArchivoTests>(_BuscarArchivoTests_QNAME, BuscarArchivoTests.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarArchivoTestsPor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarArchivo_TestsPor")
    public JAXBElement<BuscarArchivoTestsPor> createBuscarArchivoTestsPor(BuscarArchivoTestsPor value) {
        return new JAXBElement<BuscarArchivoTestsPor>(_BuscarArchivoTestsPor_QNAME, BuscarArchivoTestsPor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarArchivoTestsPorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarArchivo_TestsPorResponse")
    public JAXBElement<BuscarArchivoTestsPorResponse> createBuscarArchivoTestsPorResponse(BuscarArchivoTestsPorResponse value) {
        return new JAXBElement<BuscarArchivoTestsPorResponse>(_BuscarArchivoTestsPorResponse_QNAME, BuscarArchivoTestsPorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarArchivoTestsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "buscarArchivo_TestsResponse")
    public JAXBElement<BuscarArchivoTestsResponse> createBuscarArchivoTestsResponse(BuscarArchivoTestsResponse value) {
        return new JAXBElement<BuscarArchivoTestsResponse>(_BuscarArchivoTestsResponse_QNAME, BuscarArchivoTestsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditarArchivoTest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "editarArchivo_Test")
    public JAXBElement<EditarArchivoTest> createEditarArchivoTest(EditarArchivoTest value) {
        return new JAXBElement<EditarArchivoTest>(_EditarArchivoTest_QNAME, EditarArchivoTest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditarArchivoTestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "editarArchivo_TestResponse")
    public JAXBElement<EditarArchivoTestResponse> createEditarArchivoTestResponse(EditarArchivoTestResponse value) {
        return new JAXBElement<EditarArchivoTestResponse>(_EditarArchivoTestResponse_QNAME, EditarArchivoTestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarArchivoTest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "eliminarArchivo_Test")
    public JAXBElement<EliminarArchivoTest> createEliminarArchivoTest(EliminarArchivoTest value) {
        return new JAXBElement<EliminarArchivoTest>(_EliminarArchivoTest_QNAME, EliminarArchivoTest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarArchivoTestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Webservices/", name = "eliminarArchivo_TestResponse")
    public JAXBElement<EliminarArchivoTestResponse> createEliminarArchivoTestResponse(EliminarArchivoTestResponse value) {
        return new JAXBElement<EliminarArchivoTestResponse>(_EliminarArchivoTestResponse_QNAME, EliminarArchivoTestResponse.class, null, value);
    }

}
