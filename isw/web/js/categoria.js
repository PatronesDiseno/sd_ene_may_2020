function vistaIndividual(id){
    window.location.href = "/isw/catalogos/categoria/categoriaVistaIndividual.jsp?id="+id;
}

function cancelar(){
    window.history.back();
}

function editar(id){
    window.location.href = "/isw/catalogos/categoria/categoriaVistaFormulario.jsp?id="+id;
}

function guardar(){
    document.getElementById("formulario").submit();
}

function agregar(){
     window.location.href = "/isw/catalogos/categoria/categoriaVistaFormulario.jsp?id=-1";
}

function eliminar(id){
    if(confirm("Realmente desea eliminar el registro?")){
        window.location.href = "/isw/CategoriaServlet?id="+id+"&opcion=eliminar";
    }
}