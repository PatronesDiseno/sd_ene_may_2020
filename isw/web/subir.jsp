<%-- 
    Document   : subir
    Created on : 3/04/2020, 06:07:14 PM
    Author     : jorge
--%>

<%
    java.util.List<Archivo_TestWS.ArchivoTest> archivos = null;
    
    try {
	Archivo_TestWS.ArchivoTestWS_Service service = new Archivo_TestWS.ArchivoTestWS_Service();
	Archivo_TestWS.ArchivoTestWS port = service.getArchivoTestWSPort();
	// TODO process result here
	archivos = port.buscarArchivoTests();
    } catch (Exception ex) {
	// TODO handle custom exceptions here
    }
%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="/isw/SubirServlet" method="POST" enctype="multipart/form-data">
            <label>Seleccione un archivo</label>
            <input type="file" name="archivo" multiple="false"/>
            <input type="submit" value="Subir Archivo"/>

        </form>
        
        
        <table>
            <%
                if(archivos != null ){
                    for(Archivo_TestWS.ArchivoTest archivo : archivos){
            %>
            <tr>
                <td>
                    <a target="_blank" href="descarga.jsp?id=<%= archivo.getId() %>"><%= archivo.getNombre()%></a>
                </td>                
            </tr>
            
            <%
                
                if(archivo.getContenttype().equals("video/mp4")){
                    %>
                    <video controls="true" width="300px" height="auto">
                         <source src="descarga.jsp?id=<%= archivo.getId() %>">
                        <!--<source type="</%=archivo.getContenttype()%>" src="data:</%=archivo.getContenttype()%>;base64,</%=archivo.getArchivo() %>">-->
                    </video>
                    <%
                }
                    }
                }
            %>
            
        </table>
        
    </body>
</html>
