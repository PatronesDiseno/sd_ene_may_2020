<%-- 
    Document   : categoriaVistaIndividual
    Created on : 27/03/2020, 06:14:02 PM
    Author     : jorge
--%>


<%
    
    int id = 0;
    CategoriaWS.Categoria categoria = null;
    
    try {
        id = Integer.valueOf( request.getParameter("id"));
       
	CategoriaWS.CategoriaWS_Service service = new CategoriaWS.CategoriaWS_Service();
	CategoriaWS.CategoriaWS port = service.getCategoriaWSPort();
        
	categoria = port.buscarCategoriaId(id);
	
    } catch (Exception e) {
    }


    
%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="../../css/catalogos/categoria/categoria.css" rel="stylesheet" type="text/css"/>
        <script src="../../js/categoria.js" type="text/javascript"></script>
    </head>
    <body>
        
        <div class="contenedor-menu">
            <table>
                <tr>
                    <td colspan="3" class="direccion">
                        / isw / catalogos / categoria / Vista Individual 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a onclick="editar('<%= categoria != null ? categoria.getId() : ""  %>')" class="boton">Editar</a>
                    </td>
                    <td>
                        <a onclick="eliminar('<%= categoria != null ? categoria.getId() : ""  %>')" class="boton">Eliminar</a>
                    </td>
                    <td>
                        <a onclick="cancelar()" class="boton">Cancelar</a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="contenedor-principal">
            
            <div class="contenedor-targeta">
                <div class="header">
                    <%= categoria != null ? categoria.getNombre(): "N/A"  %>
                </div>
                <div class="contenido">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Nombre:</td>
                            <td>&nbsp;</td>
                            <td><%= categoria != null ? categoria.getNombre(): "N/A"  %></td>
                        </tr>
                        
                        <tr>
                            <td>Descripción: </td>
                            <td>&nbsp;</td>
                            <td><%= categoria != null ? categoria.getDescripcion(): "N/A"  %></td>
                        </tr>
                    </table>
                
                </div>
                <div class="footer">
                    &nbsp;
                </div>
                    
            </div>
            
            
        </div>
        
        
    </body>
</html>
