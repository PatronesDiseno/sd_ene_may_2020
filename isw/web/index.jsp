<%-- 
    Document   : index
    Created on : 10/02/2020, 02:42:01 PM
    Author     : jorge
--%>



<%@page import="operaciones.Seguridad"%>
<%
    //Checar si hay cookies
    loginWS.Usuario ucookie = new loginWS.Usuario();
    
    if(request.getCookies() != null){
        for(Cookie c: request.getCookies()){
       
        if(c != null && !c.getName().equals("JSESSIONID")){
            try {
                  
                String valor =Seguridad.decrypt(c.getValue());
                if(c.getName().equals("usuario")){
                    ucookie.setUsername(valor );
                }
                if(c.getName().equals("password")){
                    ucookie.setPassword(valor);
                }
                if(c.getName().equals("nombre")){
                    ucookie.setFullname(valor);
                }
                if(c.getName().equals("email")){
                    ucookie.setEmail(valor);
                }
                if(c.getName().equals("id")){
                    ucookie.setId(Integer.valueOf(valor));
                }  
            } catch (Exception e) {
            }
        }
    }
    //Se valida la existencia del usuario
    if(ucookie.getId() > -1 && ucookie.getFullname() != null && !ucookie.getFullname().isEmpty()){
        session.setAttribute("usuario", ucookie);
    }
    }
    
    

    //Revisar si hay session
    if(session != null &&
                    session.getAttribute("usuario") != null){                
        response.sendRedirect("bienvenido.jsp");
    }
    
    //Revisar si hay parametros
    if(request.getParameter("usuario") != null && 
            request.getParameter("password") != null){
             loginWS.Usuario user = null;
            try {
                loginWS.Login_Service service = new loginWS.Login_Service();
                loginWS.Login port = service.getLoginPort();
                 // TODO initialize WS operation arguments here
                java.lang.String username = request.getParameter("usuario");
                java.lang.String password = request.getParameter("password");
                // TODO process result here
                user = port.login(username, password);
                out.print("Return =>"+user.getFullname());
               
            } catch (Exception ex) {
                out.print("ex =>"+ex.getMessage());
            }


        
        //Revisar si está activa la opcion remember
        if(Boolean.valueOf(request.getParameter("remember")) &&
                user != null){
            //Generar las cookies
            Cookie cusuario = new Cookie("usuario",Seguridad.encrypt(user.getUsername()) );
           // Cookie cpassword = new Cookie("password",Seguridad.encrypt(user.getPassword()));
            Cookie cnombre = new Cookie("nombre",Seguridad.encrypt(user.getFullname()));
            Cookie cemail = new Cookie("email",Seguridad.encrypt(user.getEmail()));
            Cookie cid = new Cookie("id",Seguridad.encrypt(user.getId()+""));
            
            //Asignar Maxima duracion
            int tiempo = 60*5;
            cusuario.setMaxAge(tiempo);
          //  cpassword.setMaxAge(tiempo);
            cnombre.setMaxAge(tiempo);
            cemail.setMaxAge(tiempo);
            cid.setMaxAge(tiempo);
            
            //Asignar al navegador
            response.addCookie(cusuario);
            //response.addCookie(cpassword);
            response.addCookie(cnombre);
            response.addCookie(cemail);
            response.addCookie(cid);
            
        }
        
        //Añadir usuario a session
        if(user != null){
            session.setAttribute("usuario", user);
            response.sendRedirect("bienvenido.jsp");
        }
        
    }
    
%>

         

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <form >
                        <div class="container">
                            <div class="row">
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-10">
                                    <img src="img/Logo.png" alt="" class="logo"/>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                            </div>
                            <div class="row">
                                 <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-10">
                                     <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" name="usuario" class="form-control" id="username" >
                                     </div>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-10">
                                     <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" class="form-control" id="password">
                                     </div>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                            </div>
                            <div class="row">
                               
                                
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-10">
                                     <button type="submit" class="btn btn-primary btn-login">Login</button>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-10">
                                     <div class="form-group form-check">
                                         <input type="checkbox" name="remember" value="true" class="form-check-input" id="remember">
                                        <label class="form-check-label" for="remember">Remember me</label>
                                      </div>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-10">
                                    <a href="#" class="link-forgot">Forgot your password?</a>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-8 oferta-wrap" >
                    <div class="oferta">
                        <p>We have more than 100 courses register now and take the 30% 
                            off in your first buy.</p>
                        <a href="registro.jsp" class="btn btn-danger" >Register Now!!</a>
                    </div>
                    <img src="img/Oferta.jpg" class="oferta-img" alt=""/>
                </div>
            </div>
        </div>  
       
 
 

    </body>
</html>
