<%-- 
    Document   : logout
    Created on : 26/02/2020, 02:32:47 PM
    Author     : jorge
--%>
<%
    
 session.setAttribute("fullname", null);
 session.setAttribute("email", null);
 session.setAttribute("id", null);
 session.setAttribute("username", null);
 
 if(request.getCookies() != null){
        for(Cookie c: request.getCookies()){
            c.setMaxAge(0);
            response.addCookie(c);
        }
}
        
        
 session.invalidate();
 response.sendRedirect("index.jsp");

%>