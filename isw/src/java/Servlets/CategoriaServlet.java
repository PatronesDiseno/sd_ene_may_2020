/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import CategoriaWS.CategoriaWS_Service;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author jorge
 */
public class CategoriaServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_10080/isw-webservices/CategoriaWS.wsdl")
    private CategoriaWS_Service service;


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       String opcion = request.getParameter("opcion");
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            int id = Integer.valueOf(request.getParameter("id"));
            boolean resultado = false;
            
            if(opcion.equals("agregar")){
               resultado =  agregarCategoria(nombre, descripcion);
               if(resultado){ //Vista Tabla
                   response.sendRedirect("/isw/catalogos/categoria/categoriaVistaTabla.jsp");
               }else{ // Vista Formulario
                   response.sendRedirect("/isw/catalogos/categoria/categoriaVistaFormulario.jsp?id="+id+"&error=true");
               }
            }else if(opcion.equals("editar")){
               resultado =  editarCategoria(nombre, descripcion, id);
               if(resultado){ //Vista Tabla
                   response.sendRedirect("/isw/catalogos/categoria/categoriaVistaTabla.jsp");
               }else{ // Vista Formulario
                   response.sendRedirect("/isw/catalogos/categoria/categoriaVistaFormulario.jsp?id="+id+"&error=true");
               }
            }else if(opcion.equals("eliminar")){
               resultado =  eliminarCategoria(id);
               if(resultado){ //Vista Tabla
                   response.sendRedirect("/isw/catalogos/categoria/categoriaVistaTabla.jsp");
               }else{ // Vista Formulario
                   response.sendRedirect("/isw/catalogos/categoria/categoriaVistaIndividual.jsp?id="+id+"&error=true");
               }
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean editarCategoria(java.lang.String nombre, java.lang.String descripcion, int id) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        CategoriaWS.CategoriaWS port = service.getCategoriaWSPort();
        return port.editarCategoria(nombre, descripcion, id);
    }

    private boolean agregarCategoria(java.lang.String nombre, java.lang.String descripcion) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        CategoriaWS.CategoriaWS port = service.getCategoriaWSPort();
        return port.agregarCategoria(nombre, descripcion);
    }

    private boolean eliminarCategoria(int id) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        CategoriaWS.CategoriaWS port = service.getCategoriaWSPort();
        return port.eliminarCategoria(id);
    }


}
