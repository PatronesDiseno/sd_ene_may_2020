/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Archivo_TestWS.ArchivoTestWS_Service;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author jorge
 */
public class SubirServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_10080/isw-webservices/Archivo_TestWS.wsdl")
    private ArchivoTestWS_Service service;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        InputStream _inputStream = null;
        String _contentType = null; 
        String _nombre = null;
        //Los archivos se reciben por medio de part
        Part filePart = request.getPart("archivo");
        
        
        if(filePart != null){
            
            _nombre = filePart.getSubmittedFileName();
            _contentType = filePart.getContentType();
            _inputStream = filePart.getInputStream();
            
        }
        
        if(_inputStream != null){
            byte[] archivo = new byte[_inputStream.available()];
            
            _inputStream.read(archivo, 0, archivo.length);
            
            
            String _archivo = DatatypeConverter.printBase64Binary(archivo);
            
            agregarArchivoTest(_nombre, _contentType, _archivo);
            
        }
        
        response.sendRedirect("/isw/subir.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean agregarArchivoTest(java.lang.String nombre, java.lang.String contenttype, java.lang.String archivo) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        Archivo_TestWS.ArchivoTestWS port = service.getArchivoTestWSPort();
        return port.agregarArchivoTest(nombre, contenttype, archivo);
    }

}
