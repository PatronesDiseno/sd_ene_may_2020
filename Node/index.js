const express = require('express');
const app = express();

const bodyParse = require('body-parser');

//Configuración 
app.set('port', process.env.PORT || 3000);

//CORS
app.set(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParse.urlencoded({ extended: false }));
app.use(bodyParse.json());

// Aqui termina configuracion

//Inicia APP

//CRUD  CREATE = POST  READ = GET   UPDATE = PUT   DELETE = DELETE

//routes
require('./src/routes/user.route')(app);



//Termina APP

//Ejecutamos el servidor
app.listen(app.get('port'), () => {
    console.log("El servidor está corriendo en el puerto: " + app.get('port'));
});