const mysql = require('mysql');

let bd = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'academy'
});

module.exports = bd;