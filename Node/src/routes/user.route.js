const userModel = require('../models/user.model');


module.exports = function(app) {

    let user = {
        nombre: "Usuario de prueba",
        edad: 22,
        email: "prueba@gmail.com"
    };

    app.get('/users', (req, res) => {

        userModel.obtenerUsuarios((error, datos) => {
            res.status(200).json(datos);
        });


    });


    app.post('/users', (req, res) => {

        let userPost = {
            id: req.body.id,
            nombre: req.body.nombre,
            email: req.body.email
        };


        console.log('Post de usuario = ', userPost);
        res.status(200).json(userPost);
    });


    app.put('/users/:id', (req, res) => {
        let userPut = {
            id: req.params.id,
            nombre: req.body.nombre,
            email: req.body.email
        };

        console.log('Actualizando usuario = ', userPut);
        res.status(200).json(userPut);

    });

    app.delete('/users/:id', (req, res) => {

        let id = req.params.id;

        console.log('Eliminado de usuario = ', id);
        res.status(200).json({});

    });


}